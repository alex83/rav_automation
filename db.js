const { MongoClient } = require('mongodb')
const uri = 'mongodb+srv://yurikbv:tww010939@cluster0.drsxc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
if (!uri) {
  throw new Error('Missing MONGO_URI')
}

const client = new MongoClient(uri)
async function connect() {
  // Connect the client to the server
  await client.connect()

  return client.db('myFirstDatabase')
}

async function disconnect() {
  // Ensures that the client will close when you finish/error
  await client.close()
}

module.exports = { connect, disconnect }