/// <reference types="cypress" />

import { loginToApplication } from "../../support/page_objects/loginToApplication";
import { navigateTo } from "../../support/page_objects/navigationPage";
import { workingWithAPI } from "../../support/page_objects/workingWithAPI";
import { workingWithProjects } from "../../support/page_objects/workingWithProjects";
import { workingWithNotes } from "../../support/page_objects/workingWithNotes";
import { workingWithShareOption } from "../../support/page_objects/workingWithShareOption";
import { workingWithStyles } from "../../support/page_objects/workingWithStyles";
import { workingWithSubmitForEdits } from "../../support/page_objects/workingWithSubmitForEdits";

//cy.config('responseTimeout', 100000);

describe("Smoke test suite", () => {
  beforeEach(() => {
    //Log in application under 'alex55@gmail.com' user
    loginToApplication.loginUsingTestAutomationUser();
  });

  it("rav-1:Verify adding Project in Draft status", () => {
    //Click on the Start New Project button
    workingWithProjects.clickOnNewProject();
    //Click on the Upload media
    workingWithProjects.clickOnUploadButton();
    //Upload a new file
    workingWithProjects.uploadNewVideoFile(Cypress.env("videoFileName"));
    cy.wait(10000);
    //fill in Style Inspiration
    workingWithStyles.addLinkToSite(Cypress.env("styleLinkToMySite"));
    workingWithStyles.closeStyleWindow();
    //update with 'DraftProject name
    workingWithProjects.updateProjectName(
      Cypress.env("projectDraftProjectName")
    );

    //open Projects page and verify the project is added
    navigateTo.projectPage();
    workingWithProjects.openLastProject();
    workingWithProjects.verifyProjectName(
      Cypress.env("projectDraftProjectName")
    );
    //Post-condition: delete the project created
    navigateTo.projectPage();
    workingWithProjects.deleteLastProject();
  });

  it("rav-3:Verify editing Projects with 'Draft' status", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(9);

    //verify Draft project
    workingWithProjects.verifyProjectContent(
      0,
      Cypress.env("linkToDraftProjectFile")
    );

    //add Note
    workingWithNotes.addNotes(Cypress.env("noteText"));
    //verify the note is counted and added
    workingWithNotes.verifyNoteIsAdded(
      Cypress.env("noteText"),
      Cypress.env("noteCountDraftProject")
    );
    //close Pinned Notes
    workingWithNotes.closePinnedNotes();

    //verify Style Inspiration dialog
    workingWithStyles.openStyles();
    workingWithStyles.addLinkToSite(Cypress.env("styleLinkToMySite"));
    workingWithStyles.verifyPlatformIsSelected(1); //0 - tiktok, 1 - youtube, 2 - instagram
    workingWithStyles.verifyTemplateIsSelected(0); //0 - Conference, 1 - Drone etc
    workingWithStyles.closeStyleWindow();

    //Check share option
    workingWithShareOption.openShareDialog();
    workingWithShareOption.verifyDisabledSharedoption(
      Cypress.env("messageForDisabledSharedOption")
    );
    workingWithShareOption.closeDisabledSharedDialog();

    //verify to open 'Submit' window
    workingWithSubmitForEdits.verifySubmitIsAvailable();
  });

  it("rav-4:Verify submitting the project (moving to In Progress status)", () => {});

  it("rav-5:Verify editing Projects with 'In Progress' status", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(3);

    //verify In Progress project
    workingWithProjects.verifyProjectContent(
      0,
      Cypress.env("linkToInProgressProjectFile")
    );

    //add Note
    workingWithNotes.verifyExitingNoteAdded(
      Cypress.env("noteTextWithSpecialSymbols")
    );
    workingWithNotes.verifyNoteIsAdded(
      Cypress.env("noteTextWithSpecialSymbols"),
      Cypress.env("noteCountDraftProject")
    );
    workingWithNotes.closePinnedNotes();

    //Check share option
    workingWithShareOption.openShareDialog();
    workingWithShareOption.verifyDisabledSharedoption(
      Cypress.env("messageForDisabledSharedOption")
    );
    workingWithShareOption.closeDisabledSharedDialog();

    //verify to open 'Submit' window
    workingWithSubmitForEdits.verifySubmitDisabled();
  });

  it("rav-6:Verify editing Project in “Complete” status", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(4);

    //verify if content is playing - not working for huge video files
    // workingWithAPI.sendGetRequest('https://provid3.s3.us-east-2.amazonaws.com/tasks/284/1652376918_Youtube Content_1_Revised.mp4');

    //verify Complete project
    workingWithProjects.verifyMainVideoContent(
      Cypress.env("linkToCompleteProjectFileMainScreen")
    );
    workingWithProjects.verifyProjectContent(
      0,
      Cypress.env("linkToCompleteProjectFileContent")
    );
    //verify Revision section
    workingWithProjects.verifyProjectContent(
      1,
      Cypress.env("linkToCompleteProjectFileRevision")
    );

    //add Note
    workingWithNotes.addNoteForCompleteProject(Cypress.env("noteText"));
    workingWithNotes.verifyNoteIsAdded(
      Cypress.env("noteText"),
      Cypress.env("noteCountCompleteProject")
    );
    workingWithNotes.closePinnedNotes();

    //Check share option
    workingWithShareOption.openShareDialog();
    workingWithShareOption.verifySharedOptions();
    workingWithShareOption.openShareDialog(); //close it

    //verify to open 'Submit' window
    workingWithSubmitForEdits.verifySubmitIsAvailable();
  });

  it("rav-7:Verify when editing Project “In Revision”", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(2);

    //verify if content is playing - it's commented as this is bug#MVP-206
    //workingWithAPI.sendGetRequest('https://provid3.s3.us-east-2.amazonaws.com/tasks/270/1652214902_1650826293_Screen Recording How to 1.mp4');

    //verify main video content
    workingWithProjects.verifyMainVideoContent(
      Cypress.env("linkToRevisionProjectFileMainScreen")
    );
    workingWithProjects.verifyProjectContent(
      0,
      Cypress.env("linkToRevisionProjectFileContent")
    );

    //verify Revision section
    workingWithProjects.verifyProjectContent(
      1,
      Cypress.env("linkToRevisionProjectFileRevision")
    );

    //add Note
    workingWithNotes.verifyAddNoteDisabled();
    workingWithNotes.verifyNoteIsAdded(
      Cypress.env("noteTextForRevisionProject"),
      Cypress.env("noteCountRevisionProject")
    );
    workingWithNotes.closePinnedNotes();

    //Check share option
    workingWithShareOption.openShareDialog();
    workingWithShareOption.verifyDisabledSharedoption(
      Cypress.env("messageForDisabledSharedOption")
    );
    workingWithShareOption.closeDisabledSharedDialog();

    //verify to open 'Submit' window
    workingWithSubmitForEdits.verifySubmitDisabled();
  });

  it("rav-8:Verify moving the project from 'In Revision' to 'Complete' status", () => {});

  it("rav-9:Verify editing Project in “Done” status", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(0);

    //verify Done project
    workingWithProjects.verifyMainVideoContent(
      Cypress.env("linkToDoneProjectFileMainScreen")
    );
    workingWithProjects.verifyProjectContent(
      0,
      Cypress.env("linkToDoneProjectFileContent")
    );
    workingWithProjects.verifyProjectContent(
      1,
      Cypress.env("linkToDoneProjectFileRevision")
    );

    //add Note
    workingWithNotes.verifyAddNoteDisabled();
    //workingWithNotes.verifyNotesDisabled(Cypress.env('noteNOText')); - commented as comments should be reseted

    //Check share option
    workingWithShareOption.openShareDialog();
    workingWithShareOption.verifySharedOptions();
    workingWithShareOption.openShareDialog(); //click on it to close it

    //verify to open 'Submit' window
    workingWithSubmitForEdits.verifySubmitDisabled();
  });

  it('rav-10:Verify downloading video file - moving Project to "Done"', () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(1); //open 'Done' project

    //download file
    workingWithShareOption.openShareDialog();
    workingWithShareOption.downloadFile();
    //check Project status = Done
    const projectID = Cypress.env("projectIDInDone");
    cy.task("findProject", projectID).then((result) => {
      expect(result).to.equal(Cypress.env("projectDoneStatus"));
    });
    //check API returns success
    //cy.intercept('GET', 'https://staging.rav.ai/api/downloadStatus/627613e280a427e5c7a6f434').its('response.statusCode').should('eq', 503);
  });

  it('rav-11:Verify uploading video file to YouTube - moving Project to "Done"', () => {
    //open Projects tab
    //cy.visit('https://myvideospro.com')
    //navigateTo.projectPage();
    //navigateTo.testedProject(1); //open 'Done' project
    //iupload file to YouTube
    //workingWithShareOption.openShareDialog();
    //workingWithShareOption.shareViaYouTube();
    //workingWithAPI.checkDownloadFile('https://provids-staging.herokuapp.com/api/downloadStatus/627614aa80a427e5c7a6f487');
  });

  it("rav-15:Verify deleting project with 'Draft' status on the Dashboard page", () => {});

  it("rav-16:Verify deleting project with 'Draft' status on the Projects page", () => {});

  it("rav-18:Verify deleting project with 'In progress' status on the Projects page", () => {});

  it("rav-19:Verify deleting project with 'Complete' status on the Projects page", () => {});

  it("rav-21:Verify deleting project with 'In Revision' status on the Projects page", () => {});

  it("rav-23:Verify deleting project with 'Done' status on the Projects page", () => {});

  it("rav-25:Verify Playback video for Revision and Complete projects", () => {
    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(10); //open 'In Revision' project

    //verify if content is playing - not working for porojects 'in revision' status https://ravai.atlassian.net/browse/MVP-206
    workingWithAPI.sendGetRequest(Cypress.env("linkToPlayRevisionProject"));

    //open Projects tab
    navigateTo.projectPage();
    navigateTo.testedProject(11); //open 'Complete' project

    //verify if content is playing - not working for huge video files
    workingWithAPI.sendGetRequest(Cypress.env("linkToPlayCompleteProject"));
  });

  it("rav-26:Verify renaming a new Project added", () => {});
});
