/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
/*
const MongoClient = require('mongodb').MongoClient;
module.exports = (on, config) => {
  on('task', {
    updateTask(id) {
      return new Promise((resolve) => {
        MongoClient.connect(
          'mongodb+srv://yurikbv:tww010939@cluster0.drsxc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
          (err, client) => {
            if (err) {
              console.log(`MONGO CONNECTION ERROR: ${err}`);
              throw err;
            } else {
              const db = client.db('myFirstDatabase');
              //db.collection('projects').find({project_id: "627614aa80a427e5c7a6f487"}).then(res => {})
              db.collection('projects').count({}, function (error, numOfDocs) {
              //  db.collection('projects').find({project_id: "627614aa80a427e5c7a6f487"}, function (error, result) {

                resolve({ success: result });
                console.log(result);
                client.close();
            });
            }
          }
        );
      }); // end of return Promise
    },
  }); // end of task
};*/

const { connect } = require('../../db')

module.exports = async (on, config) => {
  const db = await connect()
  const project = db.collection('projects')

  on('task', {
    async findProject(projectID) {
      console.log('find the project')
      const result = await project.find({project_id: projectID}).toArray();
      const projectStatus = result[0].projectStatus;
      //const result = await project.count({})
      console.log('Result = '+result);
      console.log('Status = '+projectStatus);
      return projectStatus
    },
  })
}
