export class WorkingWithStyles {
  openStyles() {
    cy.get('.video__indicators').find('img').click();
  }
  addLinkToSite(link) {
    cy.get('.style__modal').find('input').clear().type(link);
  }

  verifyPlatformIsSelected(paltform) {
    cy.get('[class="connectSocial__links px-1.5"]')
      .find('[class="modal__payments--item w-[24%]"]')
      .eq(paltform)
      .find('img')
      .should(
        'have.attr',
        'src',
        '/static/media/accept_added_check_complite_yes_icon.78076f919fd847dfda123985cd8eb489.svg'
      );
  }
  verifyTemplateIsSelected(item) {
    cy.get('.applicable_template')
      .find('.template_modal')
      .eq(item)
      .find('img')
      .should(
        'have.attr',
        'src',
        '/static/media/accept_added_check_complite_yes_icon.78076f919fd847dfda123985cd8eb489.svg'
      );
  }

  closeStyleWindow() {
    cy.get('.pay__modal--submit').click();
  }
}

export const workingWithStyles = new WorkingWithStyles();
