export class WorkingWithAPI {
  sendGetRequest(URL){
    cy.request(
        'GET',
        URL
      ).then((response) => {
        expect(response.status).to.equal(200);
      })
  }

  checkDownloadFile(URL){
    cy.intercept('GET', URL).as('download');
    //cy.wait(30000);
    cy.wait('@download').its('response.statusCode').should('eq', 200);
    
  }

}

export const workingWithAPI = new WorkingWithAPI();
