
export class NavigationPage{
    projectPage(){
    cy.get(Cypress.env('selectorLeftPanelProjects')).click();
    cy.wait(500);
    }

    testedProject(item){
        cy.get(Cypress.env('selectorSelectProject')).eq(item).click();
    }

}

export const navigateTo = new NavigationPage();