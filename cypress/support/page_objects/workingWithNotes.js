export class WorkingWithNotes {
  addNotes(note) {
    cy.contains(Cypress.env("selectorAddNoteButton")).click();
    cy.get(Cypress.env("selectorAddNoteTextField"))
      .clear()
      .type(note + "{Enter}");
    //cy.contains('Save').click();
  }

  verifyNoteIsAdded(note, count) {
    cy.get(Cypress.env("selectorNoteIndicatorIcon"))
      .invoke("prop", "textContent")
      .should("contain", count);

    cy.get(Cypress.env("selectorNoteIndicatorIcon")).click();
    cy.get(Cypress.env("selectorNotePinnedNotesPanel"))
      .find(Cypress.env("selectorPinnedPanelTextNote"))
      .should("contain", note);
  }

  verifyExitingNoteAdded(note) {
    cy.contains(Cypress.env("selectorAddNoteButton")).click();
    cy.get(Cypress.env("selectorAddNoteTextField")).should("contain", note);
  }

  verifyAddNoteDisabled() {
    cy.contains(Cypress.env("selectorAddNoteButton")).should(
      "have.attr",
      "style",
      "padding: 5px 0px; background-color: gray;"
    );
  }
  verifyNotesDisabled(warning) {
    cy.get(Cypress.env("selectorAddNoteDisabledButton"))
      .find("span")
      .should("contain", warning);
  }

  addNoteForCompleteProject(note) {
    cy.contains(Cypress.env("selectorAddNoteButton")).click();
    cy.get(Cypress.env("selectorAddNoteFieldOnTimeline"))
      .clear()
      .type(note + "{enter}");
    // cy.get('.comment__inner').type('{enter}');
  }

  closePinnedNotes() {
    cy.get(Cypress.env("selectorNoteIndicatorIcon")).click();
  }
}

export const workingWithNotes = new WorkingWithNotes();
