export class WorkingWithShareOption {
  openShareDialog() {
    cy.get('.share_indicator').click();
  }
  verifyDisabledSharedoption(warning) {
    cy.get(
      '[class="warning p-[17px] absolute right-0 ml-0 md:ml-[42px] min-w-[150px] md:min-w-[222px]"]'
    ).should('contain', warning);
  }

  closeDisabledSharedDialog() {
    cy.get(
      '[class="delete__video--btn w-[1.20rem] h-[1.20rem] border-none bg-transparent text-[14px] z-[100] right-[3px] top-[-22px] md:top-[-6px]"]'
    ).click();
  }

  verifySharedOptions() {
    cy.get('.ShareModal').should('contain', 'Share to');
    cy.get('.ShareModal__buttons--download')
      .find('button')
      .invoke('prop', 'disabled')
      .should('equal', false);
    cy.get('.ShareModal__buttons--instagram')
      .find('button')
      .invoke('prop', 'disabled')
      .should('equal', false);
    cy.get('.ShareModal__buttons--youtube')
      .find('button')
      .invoke('prop', 'disabled')
      .should('equal', false);
    cy.get('.ShareModal__buttons--tiktok')
      .find('button')
      .invoke('prop', 'disabled')
      .should('equal', false);
    cy.get('.ShareModal__buttons--onlyFans')
      .find('button')
      .invoke('prop', 'disabled')
      .should('equal', false);
  }

  downloadFile() {
    cy.get('.ShareModal').find('.ShareModal__buttons--download').click();
  }

  shareViaYouTube() {
    cy.get('.ShareModal').find('.ShareModal__buttons--youtube').click();
  }
}

export const workingWithShareOption = new WorkingWithShareOption();
