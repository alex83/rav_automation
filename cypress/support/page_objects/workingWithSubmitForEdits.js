export class WorkingWithSubmitForEdits{
    verifySubmitIsAvailable(){
        cy.contains('Submit for Edits').click();
        cy.get('.massage').should(
          'contain',
          "You are about to submit your project for edits. You won't be able to update while it is being processed. Are you sure?"
        );
        cy.get('.connectSocial__cross').click();
    }
    verifySubmitDisabled(){
        cy.get('.generate-video').should(
            'have.attr',
            'style',
            'padding: 5px 0px; background-color: gray;'
          );
    }

}

export const workingWithSubmitForEdits = new WorkingWithSubmitForEdits();