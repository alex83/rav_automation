export class WorkingWithProjects {
  verifyProjectContent(item, content) {
    cy.get(".slider-list")
      .eq(item)
      .find("video")
      .invoke("prop", "currentSrc")
      .should("contain", content);
  }

  verifyMainVideoContent(videoSource) {
    cy.get(".video__inner")
      .find("video")
      .invoke("prop", "currentSrc")
      .should("contain", videoSource);
  }

  clickOnNewProject() {
    cy.contains("Start New Project").click();
  }

  clickOnUploadButton() {
    cy.get('[for="upload__media--button"]').click();
  }

  uploadNewVideoFile(fileName) {
    cy.get(".dropzone_css")
      .eq(0)
      .selectFile("cypress/fixtures/video files/" + fileName, {
        action: "drag-drop",
      });
  }

  updateProjectName(projectName) {
    cy.get(
      '[class="text-[20px] md:text-[20px] font-bold text-[#3b8590] focus:outline-gray-200 px-2 max-h-[50px] md:max-h-fit  overflow-auto"]'
    )
      .clear()
      .type(projectName + "{enter}");
  }

  openLastProject() {
    cy.get('[class="project__item w-5/6 md:w-[18%] xl:w-[19%]"]')
      .last()
      .click();
  }

  verifyProjectName(projectName) {
    cy.get(
      '[class="text-[20px] md:text-[20px] font-bold text-[#3b8590] focus:outline-gray-200 px-2 max-h-[50px] md:max-h-fit  overflow-auto"]'
    ).should("contain", projectName);
  }

  deleteLastProject(){
    cy.get('[class="delete__video--btn"]')
    .last()
    .click();
  }
}

export const workingWithProjects = new WorkingWithProjects();
