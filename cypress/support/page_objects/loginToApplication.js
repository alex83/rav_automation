export class LoginToAppication {
  //Log in application under 'alex55@gmail.com' use

  loginUsingTestAutomationUser() {
    //log in via API request
    const userCreds = { email: Cypress.env('generalEmail'), password: Cypress.env('generalPassword') };
    cy.request(
      'POST',
      Cypress.env('generalLoginURL'),
      userCreds
    )
      .its('body')
      .then((body) => {
        const token = body.token;
        cy.visit('/', {
          onBeforeLoad(win) {
            win.localStorage.setItem('token', token);
          },
        });
      });

    /* log in by typing user creds
            cy.visit('/');
            cy.contains('Login').click();
            cy.get('.email__button').click();
            cy.get('[type="email"]').type('alex55@gmail.com');
            cy.get('[type="password"]').type('1111');
            cy.get('[type="submit"]').click();*/
  }
}

export const loginToApplication = new LoginToAppication();
